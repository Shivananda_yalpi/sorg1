public class duplicateLeadCheck {
    List<lead> existingLeads{set;get;}
    
    public duplicateLeadCheck(){
        existingLeads=[select Email from lead];
    }
    
    public void duplicateLead(String email){
        for(lead e:existingLeads){
            if(e.email==email){
                system.debug('duplicate record');
            }
        }
    }
}